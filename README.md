# MACRO Feedforward Vibration Control

This Julia code models, simulates and controls a simple, planar, slender, finite element payload with a tip mass.

##Requirements:
Julia programming language installed -> https://julialang.org/downloads/
VSCode with Julia extensions

##Installation:

From a command prompt, navigate to your chosen download folder, then run

```bash
git clone git@gitlab.com:sherschm/MACROFeedforwardVibrationControl.git
cd MACROFeedforwardVibrationControl
```

Open VSCode
```bash
code .
```

Then, run the main.jl file within src from VSCode. 

You may have to do this if there are errors

```bash
] # access the package manager
activate .
precompile 
instantiate
```

This should generate the model and simulate an optimal trajectory. See the plots
folder for an animation (FEMpayload.gif)

To toggle the control technique from the baseline to the optimal approach, 
change the ctrl_law variable between "Baseline" and "Optimal_Ffwd".

You can change payload / robot parameters by editing the payload_robot_params.jl
file

#Running the trajectories in real life

Uncontrolled (Baseline) trajectory looks like this:

![uncontrolled payload gif](./data/UnshapedExperiment.gif)

Sending the Optimal trajectory to the UR10e looks like this:

![Optimal Trajectory gif](./data/OptimalExperiment.gif)

ZVD Input shaping the optimal trajectory looks like this (Input shaping 
simulation not included in this repo):

![Optimal Trajectory gif](./data/OptimalExperimentWithZVDIS.gif)