##Rough script that generates a range of trajectories for the planar payload tip

function def_ramp(t_ramp,v_max,n,Δt)
    #Create a ramp function. Can be a position, velocity or anything.

    #t_ramp is a vector of time locations as proportion of total trajectory time.
    #v_max is the maximum value of ramp

    t_start_ind=Int(round(t_ramp[1]*n));
    t_rise_ind=Int(round(t_ramp[2]*n));

    cmnd_traj=Array{Float64}(undef,n);
    cmnd_traj_dot=Array{Float64}(undef,n);
    cmnd_traj.=0
    cmnd_traj_dot.=0
    for i in t_start_ind:t_rise_ind
        cmnd_traj[i]=(v_max/(t_rise_ind-t_start_ind))*(i-t_start_ind);
        cmnd_traj_dot[i]=(v_max/(Δt*(t_rise_ind-t_start_ind)))
    end
    for i in t_rise_ind+1:n
        cmnd_traj[i]=v_max; 
        cmnd_traj_dot[i]=0;
    end
    return cmnd_traj, cmnd_traj_dot
end

function gen_traj(traj_mode,n,Δt)
    ## This function produces a desired trajectory function of the payload tip - (x,y,theta) coords

    cmnd_traj=Array{Float64}(undef,n,3)
    cmnd_traj_dot=Array{Float64}(undef,n,3)
    cmnd_traj.=0
    cmnd_traj_dot.=0

    if traj_mode=="P2P"

        qdes(t)=[0.7;-0.6;-0.1] #Al Payload with rotation
        qdesdot(t)=[0;0;0]

        for i in 1:n
            cmnd_traj[i,:]=qdes(i*Δt)
            cmnd_traj_dot[i,:]=qdesdot(i*Δt)
        end

    elseif traj_mode=="sin"

        cmnd_traj=Array{Float64}(undef,n,3)
        cmnd_traj_dot=Array{Float64}(undef,n,3)
        for i in 1:n
            cmnd_traj[i,:]=qdes(i*Δt)
            cmnd_traj_dot[i,:]=qdesdot(i*Δt)
        end
    elseif traj_mode=="ramp"
        y_height=-0.7 #FOR AL Payload

        #t_ramp=[0.05 0.3]
        t_ramp=[0.05 0.2] #HARSH!!
        #disp_max=-0.7
        disp_max=-0.3
        cmnd_traj=[-y_height.*ones(n,1) def_ramp(t_ramp,disp_max,n,Δt)[1] zeros(n,1)]
        cmnd_traj_dot=[zeros(n,1) def_ramp(t_ramp,disp_max,n,Δt)[2] zeros(n,1)]

    elseif traj_mode=="Comp_ramp"

        y_height=-0.7 #FOR AL Payload
        t_ramp=[0.05 0.3] #OG
        #t_ramp=[0.05 0.2]
        disp_max=-0.6
        cmnd_traj=[-def_ramp(t_ramp,-0.1,n,Δt)[1].-y_height def_ramp(t_ramp,disp_max,n,Δt)[1] def_ramp(t_ramp,-0.08,n,Δt)[1]]
        cmnd_traj_dot=[-def_ramp(t_ramp,-0.1,n,Δt)[2] def_ramp(t_ramp,disp_max,n,Δt)[2] def_ramp(t_ramp,-0.08,n,Δt)[2]]

    end
    
    return cmnd_traj,cmnd_traj_dot
end
