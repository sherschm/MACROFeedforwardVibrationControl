using DemoSlenderFfwdCtrl
#using Interpolations
#using DelimitedFiles

##Load dynamic model and system parameters & functions from script
include("payload_robot_params.jl")
dynamics=(M,C,K,G)

##Choose your control law, from Baseline or Optimal_Ffwd
ctrl_law="Optimal_Ffwd"#"Baseline"#

##Simulation / Optimisation parametesrs
Δt=0.1 #Optimisation time step
tmax=6
n=Int(round(tmax/Δt))
tvec=Δt:Δt:tmax

##Choose trajectory 
traj_mode= "Comp_ramp" # Compound ramp tip trajectory array
cmd= gen_traj(traj_mode,n,Δt) 
cmd_traj,cmd_traj_dot=cmd 

##Define Initial state of payload q0, where ni = number of coordinates q. state=[q;qdot]
q0=Vector{Float64}(undef,2*ni)
q0.=0;
q0[1]=-l+0.7 #choose some initial x-position, based on robot workspace


## Set Control parameters and run simulations
if ctrl_law=="Baseline"
    ## No payload oscillation control. Simulates trajectory that assumes payload is rigid
    pos_matrix, vel_matrix, force_matrix,tvec_sim=runPD(q0,Δt,tvec,cmd_traj,cmd_traj_dot)

    #use inverse kinematics to produce a robot trajectory
    joint_matrix, joint_vel_matrix, err_vec, y_vec,x_vec, max_joint_v=UR10e_OfflineIK(pos_matrix,tvec_sim,tvec) 

    #Make plots and animation, save to plots/ folder
    FEM_plots(pos_matrix,joint_matrix,force_matrix,cmd_traj,tvec_sim) 

elseif ctrl_law=="Optimal_Ffwd"
    ##Calculate the optimal robot trajectory
    pos_matrix, vel_matrix,force_matrix,joint_matrix,joint_vel_matrix,err=PayloadOptFfwdSim(dynamics,le,q0,cmd_traj,tvec,Δt,n,robot_lims);

    #FEM_plots(pos_matrix,joint_matrix,force_matrix,cmd_traj,tvec) #Make plots and animation, save to plots/ folder

    #Simulate the payload trajectory using PD position control law
    pos_matrix_sim, vel_matrix_sim, force_matrix_sim,tvec_sim=RunEEPosControl(q0,tmax,Δt,tvec,pos_matrix,vel_matrix) 

    #interpolate the joint array and command for plotting
    joint_matrix_sim=general_lin_interp(joint_matrix',tvec,tvec_sim)'
    cmd_traj_sim=general_lin_interp(cmd_traj,tvec,tvec_sim)

    #Make plots and animation, save to plots/ folder
    FEM_plots(pos_matrix_sim,joint_matrix_sim,force_matrix_sim,cmd_traj_sim,tvec_sim) 
end