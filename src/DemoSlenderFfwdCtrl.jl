module DemoSlenderFfwdCtrl

using Plots
using DifferentialEquations
using LinearAlgebra
using Interpolations
using DelimitedFiles
using Waveforms
using DataFrames
using CSV
using DSP
using ForwardDiff
using Symbolics
using LaTeXStrings


#Optimisation Packages
using JuMP
using Ipopt #Nonlinear optimisation algorithm

include("Simulators.jl")
export runPD,FEA_PD_SIM, RunEEPosControl

include("Plotting.jl")
export FEM_plots, general_lin_interp

include("KinematicsFuncs.jl")
export UR10e_OfflineIK

include("Trajectories.jl")
export gen_traj

include("NMPC.jl")
export PayloadOptFfwdSim

include("InputShaping.jl")
export joint_input_shaper

include("FEModelingFuncs.jl")

include("payload_robot_params.jl")

export Slender_beam_FEM
end
