

function q_fnc(x::Vector)
    #Outputs x from state vector: [x;xdot]
    n=Int(length(x)/2)
    out=x[1:n]
    return out
end

function qdot_fnc(x::Vector)
    #Outputs xdot from state vector: [x;xdot]
    n=Int(length(x)/2)
    out=x[n+1:2*n]
    return out
end

function PayloadOptFfwdSim(dynamics,le,q0,cmd_traj,tvec,Δt,n,robot_lims)

    ##turn tip command matrix into time function
    cmd_matrix=zeros(n,3)
    for i in 1:3
        interp_linear_extrap_cmd=LinearInterpolation(tvec, cmd_traj[:,i],extrapolation_bc=Line()) 
        cmd_matrix[:,i]=interp_linear_extrap_cmd(tvec)
    end 

    #Run Optimisation
    force_matrix,err,pos_matrix,vel_matrix,th_matrix,thdot_matrix= MPCoptFEAwithIK(q0,Δt,cmd_traj,n)
    return  pos_matrix,vel_matrix, force_matrix, th_matrix,thdot_matrix, err 
end

function runPD(q0,Δt,tvec,cmd_traj,cmd_traj_dot)
    tmax=maximum(tvec)

    x_cmd=cmd_traj[:,1]-l*cos.(cmd_traj[:,3])
    y_cmd=cmd_traj[:,2]-l*sin.(cmd_traj[:,3])
    th_cmd=cmd_traj[:,3]
    pos_cmd=[x_cmd y_cmd th_cmd]

    vel_cmd=zeros(size(pos_cmd))
    vel_cmd[2:size(pos_cmd,1),:]=(1/Δt)*diff(pos_cmd,dims=1)
  
    #No payload oscillation control. Simulates trajectory that assumes payload is rigid
    pos_matrix, vel_matrix, force_matrix,tvec_control= RunEEPosControl(q0,tmax,Δt,tvec,pos_cmd',vel_cmd')

    return pos_matrix, vel_matrix, force_matrix, tvec_control
end

function FEA_PD_SIM(q0,tf,Δt,M,C,K,G,q_des,qdot_des) 
    ## Recieves desired end effector q and qdot commands and Simulates the robot and payload response, using simple PD control.
    # Commands are just planar end-effector coordiates: q=[x,y,theta]

    #Dynamic equation
    f(x,p,t)=[qdot_fnc(x);\(M(q_fnc(x)),(-C([q_fnc(x);qdot_fnc(x)])*(qdot_fnc(x))-K*q_fnc(x)+tauPD(x,q_des,qdot_des,t)-vec(G(q_fnc(x)))))] #with coriolis
    tspan= (0,tf-Δt);

    prob=ODEProblem(f,q0,tspan);
    println("Generating response...")
    sol = solve(prob)#,Tsit5(), reltol=1e-8, abstol=1e-8)
    tvec=sol.t
    n_sim=length(tvec)

    pos_matrix=Matrix{Float64}(undef,ni,n_sim);
    vel_matrix=Matrix{Float64}(undef,ni,n_sim);
    #full_state=Matrix{Float64}(undef,2*ni,length(tvec));
    
    force_matrix=Matrix{Float64}(undef,3,n_sim);
    for i in 1:n_sim
        pos_matrix[:,i]=sol.u[i][1:ni]
        vel_matrix[:,i]=sol.u[i][ni+1:2*ni]
        #full_state[:,i]=[pos_matrix[:,i];vel_matrix[:,i]]
       # if i>1
       #     force_matrix[:,i-1]=(M*((vel_matrix[:,i]-vel_matrix[:,i-1])/(tvec[i]-tvec[i-1]))+C*(q_fnc(full_state[:,i]).^2)+K*q_fnc(full_state[:,i])+vec(G(q_fnc(full_state[:,i]))))[1:3]
       # end
       q0_force_calc=vec([pos_matrix[:,i]; vel_matrix[:,i]])
       force_matrix[:,i]=tauPD(q0_force_calc,q_des,qdot_des,tvec[i])[1:3]
    end

    return pos_matrix, vel_matrix, force_matrix, tvec
end



#EE PD control law
function tauPD(x,q_des,qdot_des,t)
    #PID control law

    Kp=[120000 0 0;0 800000 0;0 0 100000];
    Kd=[1000 0 0;0 1500 0;0 0 300];

    #Kp=[10000 0 0;0 80000 0;0 0 2000];
    #Kd=[0 0 0;0 0 0;0 0 0]

    xdes=q_des[1]
    ydes=q_des[2]
    thdes=q_des[3]
    xdes_dot=qdot_des[1]
    ydes_dot=qdot_des[2]
    thdes_dot=qdot_des[3]

    qdes_EE(t)=[xdes(t);ydes(t);thdes(t)]
    qdesdot_EE(t)=[xdes_dot(t);ydes_dot(t);thdes_dot(t)]
    
    e(x,t)=q_fnc(x)[1:3]-qdes_EE(t);
    edot(x,t)=qdot_fnc(x)[1:3]-qdesdot_EE(t);

    Forces=-Kp*e(x,t)-Kd*edot(x,t)
    ctrl_out=Vector{Float64}(undef,ni)
    ctrl_out.=0

    ctrl_out[1:3]=Forces
    #Enforce actuator limits
    #=
    if Forces[1]>Fmax
        ctrl_out[1]=Fmax
    else
        ctrl_out[1]=Forces[1]
    end
    if Forces[2]>Fmax
        ctrl_out[2]=Fmax
    else
        ctrl_out[2]=Forces[2]
    end
    if Forces[3]>Tmax
        ctrl_out[3]=Tmax
    else
        ctrl_out[3]=Forces[3]
    end =#

    return ctrl_out
end

function RunEEPosControl(q0,tf,Δt,tvec,pos_matrix,vel_matrix) 
    ## Recieves desired end effector q and qdot commands and Simulates the robot and payload response, using simple PD control.
    # Commands are just planar end-effector coordiates: q=[x,y,theta]

    xdes_dot = LinearInterpolation(tvec, vel_matrix[1,:],extrapolation_bc=Line())
    ydes_dot = LinearInterpolation(tvec, vel_matrix[2,:],extrapolation_bc=Line())
    thdes_dot = LinearInterpolation(tvec, vel_matrix[3,:],extrapolation_bc=Line())

    xdes = LinearInterpolation(tvec, pos_matrix[1,:],extrapolation_bc=Line())
    ydes = LinearInterpolation(tvec,  pos_matrix[2,:],extrapolation_bc=Line())
    thdes = LinearInterpolation(tvec,  pos_matrix[3,:],extrapolation_bc=Line())
    
    q_des=(xdes,ydes,thdes)
    qdot_des=(xdes_dot,ydes_dot,thdes_dot)

    #Dynamic equation
    f(x,p,t)=[qdot_fnc(x);\(M(q_fnc(x)),(-C([q_fnc(x);qdot_fnc(x)])*(qdot_fnc(x))-K*q_fnc(x)+tauPD(x,q_des,qdot_des,t)-vec(G(q_fnc(x)))))] #with coriolis
    #f(x,p,t)=[qdot_fnc(x);\(M(q_fnc(x)),(-K*q_fnc(x)+tauPD(x,q_des,qdot_des,t)-vec(G(q_fnc(x)))))] #without coriolis (WRONG - JUST TO TEST!)

    tspan= (0,tf-Δt);

    prob=ODEProblem(f,q0,tspan);
    println("Generating response...")
    sol = solve(prob)#,Tsit5(), reltol=1e-8, abstol=1e-8)
    tvec_sim=sol.t
    n_sim=length(tvec_sim)

    pos_matrix=Matrix{Float64}(undef,ni,n_sim);
    vel_matrix=Matrix{Float64}(undef,ni,n_sim);
    #full_state=Matrix{Float64}(undef,2*ni,length(tvec));
    
    force_matrix=Matrix{Float64}(undef,3,n_sim);
    for i in 1:n_sim
        pos_matrix[:,i]=sol.u[i][1:ni]
        vel_matrix[:,i]=sol.u[i][ni+1:2*ni]
        #full_state[:,i]=[pos_matrix[:,i];vel_matrix[:,i]]
       # if i>1
       #     force_matrix[:,i-1]=(M*((vel_matrix[:,i]-vel_matrix[:,i-1])/(tvec[i]-tvec[i-1]))+C*(q_fnc(full_state[:,i]).^2)+K*q_fnc(full_state[:,i])+vec(G(q_fnc(full_state[:,i]))))[1:3]
       # end
       q0_force_calc=vec([pos_matrix[:,i]; vel_matrix[:,i]])
       force_matrix[:,i]=tauPD(q0_force_calc,q_des,qdot_des,tvec_sim[i])[1:3]
    end
    
    #if Δt<=0.05
        pos_matrix_out =general_lin_interp(pos_matrix',tvec_sim,tvec)
        vel_matrix_out = general_lin_interp(vel_matrix',tvec_sim,tvec)
        force_matrix_out = general_lin_interp(force_matrix',tvec_sim,tvec)
        tvec_out=tvec
    #else
       # tvec_out=0.05:0.05:tf
        #pos_matrix_out =general_lin_interp(pos_matrix',tvec_sim,tvec_out)
       #vel_matrix_out = general_lin_interp(vel_matrix',tvec_sim,tvec_out)
        #force_matrix_out = general_lin_interp(force_matrix',tvec_sim,tvec_out)
   # end
    return pos_matrix_out', vel_matrix_out', force_matrix_out', tvec_out
    #return pos_matrix', vel_matrix', force_matrix', tvec_out
end

#=
function RunEEPosControlFfwd(q0,tf,Δt,tvec,F) 
    ## Recieves desired end effector q and qdot commands and Simulates the robot and payload response, using simple PD control.
    # Commands are just planar end-effector coordiates: q=[x,y,theta]

    Fx = LinearInterpolation(tvec, F[1,:],extrapolation_bc=Line())
    Fy = LinearInterpolation(tvec, F[2,:],extrapolation_bc=Line())
    Fth = LinearInterpolation(tvec, F[3,:],extrapolation_bc=Line())

    Force(t)=[Fx(t);Fy(t);Fth(t);zeros(ni-3)]

    #Dynamic equation
    #f(x,p,t)=[qdot_fnc(x);\(M(q_fnc(x)),(-C([q_fnc(x);qdot_fnc(x)])*(qdot_fnc(x))-K*q_fnc(x)+Force(t)-vec(G(q_fnc(x)))))] #with coriolis
    f(x,p,t)=[qdot_fnc(x);\(M(q_fnc(q0)),(-C(q0)*(qdot_fnc(x))-K*q_fnc(x)+Force(t)-vec(G(q_fnc(x)))))] #with coriolis

    tspan= (0,tf-Δt);
    #tspan= (0,tf+Δt); #THIS MIGHT BE WRONG!!
    prob=ODEProblem(f,q0,tspan);
    println("Generating response...")
    sol = solve(prob)#,Tsit5(), reltol=1e-8, abstol=1e-8)
    tvec_out=sol.t
    n_sim=length(tvec_out)

    pos_matrix=Matrix{Float64}(undef,ni,n_sim);
    vel_matrix=Matrix{Float64}(undef,ni,n_sim);
    #full_state=Matrix{Float64}(undef,2*ni,length(tvec));
    
    force_matrix=Matrix{Float64}(undef,3,n_sim);
    for i in 1:n_sim
        pos_matrix[:,i]=sol.u[i][1:ni]
        vel_matrix[:,i]=sol.u[i][ni+1:2*ni]
        #full_state[:,i]=[pos_matrix[:,i];vel_matrix[:,i]]
       # if i>1
       #     force_matrix[:,i-1]=(M*((vel_matrix[:,i]-vel_matrix[:,i-1])/(tvec[i]-tvec[i-1]))+C*(q_fnc(full_state[:,i]).^2)+K*q_fnc(full_state[:,i])+vec(G(q_fnc(full_state[:,i]))))[1:3]
       # end
       q0_force_calc=vec([pos_matrix[:,i]; vel_matrix[:,i]])
       #force_matrix[:,i]=Force(i*)
    end

    pos_matrix_out =general_lin_interp(pos_matrix',tvec_out,tvec)
    vel_matrix_out = general_lin_interp(vel_matrix',tvec_out,tvec)
    #force_matrix_out = general_lin_interp(force_matrix',tvec_out,tvec)

    return pos_matrix_out', vel_matrix_out'
end=#