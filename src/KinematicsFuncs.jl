##Joint Space trajectory generator
using JuMP
using Ipopt
using DataFrames
using CSV
import ForwardDiff

function UR10eFK(th)

  x_EE = link_l[1]*sin(th[1])+link_l[2]*sin(th[1]+th[2])+link_l[3]*sin(th[1]+th[2]+th[3])
  y_EE = link_l[1]*cos(th[1])+link_l[2]*cos(th[1]+th[2])+link_l[3]*cos(th[1]+th[2]+th[3])
  th_EE=th[1] + th[2] + th[3] + th[4]

  return [x_EE;y_EE;th_EE]
end

function UR10e_OfflineIK(pos_matrix,tvec_control,tvec_robot)
  #Optimisation based inverse kinematics

  ## Input: Desired end effector trajectory (x,y,theta)
  ## Output: Permissable trajectory of 4 in-plane UR10 motors.
  if size(pos_matrix,1)>1 && size(pos_matrix,2)>1
    pos_matrix_int=general_lin_interp(pos_matrix',tvec_control,tvec_robot)
    cmd_traj_EE=pos_matrix_int[:,1:3]

    Δt=tvec_robot[2]-tvec_robot[1];
  else #just a vector - only one set of joint angles is needed.
    cmd_traj_EE=pos_matrix
  end
  n= size(cmd_traj_EE,1)

  #Initialise Optimisation problem
  traj = JuMP.Model(Ipopt.Optimizer)
  JuMP.set_silent(traj)
  
  #Decision variables
  JuMP.@variables(traj, begin
    # State variables
    -5 ≤ y_EE[1:n] ≤ 5  
    -5 ≤ x_EE[1:n] ≤ 5
    -5 ≤ th_EE[1:n] ≤ 5
    err[1:n] ≥ 0   
    J[1] ≥ 0               #error
    # Control variables 
    #=
    -th_lims[1:n_mot] ≤ th[1:n_mot,1:n] ≤ th_lims[1:n_mot]
    -th_d_lims[1:n_mot] ≤ th_dot[1:n_mot,1:n] ≤ th_d_lims[1:n_mot]  
    -th_dd_lims[1:n_mot] ≤ th_ddot[1:n_mot,1:n] ≤ th_dd_lims[3]  =#
    -3*pi≤ th[1:n_mot,1:n]≤ 3*pi
    -3 ≤th_dot[1:n_mot,1:n]≤ 3 #Maximum speed is 191 deg/s ~3rad/s
    -8≤ th_ddot[1:n_mot,1:n]≤ 8
  end)

  for i in 1:n_mot
      ##ICs
      JuMP.fix(th_dot[i,1], 0; force = true)
      ##Final velocity constraints
      #JuMP.fix(th_dot[i,n], 0; force = true)
  end

  JuMP.@expressions(
      traj,
      begin
          cmdx[j = 1:n],cmd_traj_EE[j,1]
          cmdy[j = 1:n],cmd_traj_EE[j,2]
          cmdth[j = 1:n],-cmd_traj_EE[j,3]
          #cmdth[j = 1:n],cmd_traj_EE[j,3]
      end
  )
  
  ##Dynamic Contraints
  for j in 1:n
      JuMP.@constraint(traj,-0.85*pi<=th[1,j]<=-0.15*pi) #Constrain end effector angle
      JuMP.@constraint(traj, th_EE[j]==th[1,j] + th[2,j] + th[3,j] + th[4,j]) #Constrain end effector angle
      JuMP.@NLconstraint(traj, x_EE[j] == link_l[1]*sin(th[1,j])+link_l[2]*sin(th[1,j]+th[2,j])+link_l[3]*sin(th[1,j]+th[2,j]+th[3,j])) #Equation of end effector position
      JuMP.@NLconstraint(traj, y_EE[j] == link_l[1]*cos(th[1,j])+link_l[2]*cos(th[1,j]+th[2,j])+link_l[3]*cos(th[1,j]+th[2,j]+th[3,j])) #Equation of end effector position
      JuMP.@NLconstraint(traj, err[j] == (cmdy[j]-y_EE[j])^2+(cmdx[j]-x_EE[j])^2+(cmdth[j]-th_EE[j])^2) #error between demand trajectory and actual trajectory at each time point
  end
          
  for j in 2:n, i in 1:n_mot
      # Angle constraints -> theta_i_dot=d/dt(theta_i)
      JuMP.@NLconstraint(traj, th_dot[i,j] * Δt == th[i,j]-th[i,j-1])
      JuMP.@NLconstraint(traj, th_ddot[i,j] * Δt == th_dot[i,j]-th_dot[i,j-1]) 
  end
  
  #Constraint that creates the optimisation cost function J (aka the variable that we want to minimise)
  JuMP.@NLconstraint(traj, J[1]  == sum(err[j] for j in 1:n))
  
  ## Objective function
  JuMP.@objective(traj, Min, J[1])
  
  ## Solve!
  println("Generating Robot Joint Trajectories...")
  JuMP.optimize!(traj)
  JuMP.solution_summary(traj)
  
  #Create output matrix containing all joint angles for full trajectory
  th_matrix= JuMP.value.(th)[:,:];
  th_dot_matrix= JuMP.value.(th)[:,:];
  thdot_matrix= JuMP.value.(th_dot)[:,:];
  max_joint_v=maximum(thdot_matrix);
  #th_matrix=th_matrix'
  
  #error vector indicating whether trajectory is possible
  err_vec= JuMP.value.(err)[:];
  y_vec= JuMP.value.(y_EE)[:];
  x_vec= JuMP.value.(x_EE)[:];

  #Convert to UR coordinate system and export to CSV file.
  th_matrix_UR= conv2UR10_commnd(th_matrix,tvec_robot)
  URTraj=DataFrame([Array(tvec_robot)';th_matrix_UR']', :auto)
  CSV.write("data/JointAngles.csv", URTraj,header=false) #Write global sim parameters to a txt file

  th_vel_matrix=zeros(size(th_matrix))
  th_vel_matrix[:,2:size(th_matrix,2)]=(1/Δt)*diff(th_matrix,dims=2)

  return th_matrix,th_dot_matrix, err_vec, y_vec,x_vec, max_joint_v
end

function conv2UR10_commnd(th_matrix,tvec_red)
  #transform joint matrix into UR10 coordinate system
  #FOR OFFLINE TRAJECTORIES ONLY
  if size(th_matrix,2)!=4
    th_matrix=th_matrix'
  end
  th_matrix_UR=Matrix{Float64}(undef,length(tvec_red),4)
  th_matrix_UR.=0;
  th_matrix_UR[:,1]=th_matrix[:,1]
  th_matrix_UR[:,2]=th_matrix[:,2]
  th_matrix_UR[:,3]=th_matrix[:,3].-pi/2
  th_matrix_UR[:,4]=th_matrix[:,4].+pi/2

  #keep initial position of EE always facing the same way (all joints sum to -PI) This needs to be made more robust!
  while sum(th_matrix_UR[1,:])>0.5
    th_matrix_UR[:,4]=th_matrix_UR[:,4].-pi
  end
  while sum(th_matrix_UR[1,:])<-0.5
    th_matrix_UR[:,4]=th_matrix_UR[:,4].+pi
  end

  #Ensure no joints past their limits
  while maximum(th_matrix_UR[:,3])>2*pi
    th_matrix_UR[:,3]=th_matrix_UR[:,3].-2*pi
    th_matrix_UR[:,4]=th_matrix_UR[:,4].+2*pi
  end
  while maximum(th_matrix_UR[:,3])<-2*pi
    th_matrix_UR[:,3]=th_matrix_UR[:,3].+2*pi
    th_matrix_UR[:,4]=th_matrix_UR[:,4].-2*pi
  end

  return th_matrix_UR
end


function UR10eIKwithManip(EE_init_pos,cmd_traj)
  #Optimisation based inverse kinematics. 
  #Initialises UR10e joints, aiming to align its maneuvrability ellipsoid with initial direction of desired trajectory

  ## Input: Desired end effector trajectory (x,y,theta)
  ## Output: Permissable trajectory of 4 in-plane UR10 motors.

  #desired direction
  init_dir=cmd_traj[2,:]-EE_init_pos

  #Initialise Optimisation problem
  traj = JuMP.Model(Ipopt.Optimizer)
  JuMP.set_silent(traj)
  
  #Decision variables
  JuMP.@variables(traj, begin
    # State variables
    -5 ≤ y_EE ≤ 5  
    -5 ≤ x_EE ≤ 5
    -5 ≤ th_EE ≤ 5
    err ≥ 0   
    J[1] ≥ 0               #error

    -3*pi≤ th[1:n_mot]≤ 3*pi
  end)
  
  JuMP.@constraint(traj,-0.85*pi<=th[1]<=-0.15*pi) #Constrain end effector angle
  JuMP.@constraint(traj, th_EE[j]==th[1] + th[2] + th[3] + th[4]) #Constrain end effector angle
  JuMP.@NLconstraint(traj, x_EE[j] == link_l[1]*sin(th[1,j])+link_l[2]*sin(th[1]+th[2])+link_l[3]*sin(th[1]+th[2]+th[3])) #Equation of end effector position
  JuMP.@NLconstraint(traj, y_EE[j] == link_l[1]*cos(th[1,j])+link_l[2]*cos(th[1]+th[2])+link_l[3]*cos(th[1]+th[2]+th[3])) #Equation of end effector position
  JuMP.@NLconstraint(traj, err[j] == (cmd_traj[1,2]-y_EE[j])^2+(cmd_traj[1,1]-x_EE[j])^2+(-cmd_traj[1,3]-th_EE[j])^2) #error between demand trajectory and actual trajectory at each time point
          
  JuMP.@NLconstraint(traj, err[j] == (cmd_traj[1,2]-y_EE[j])^2+(cmd_traj[1,1]-x_EE[j])^2+(-cmd_traj[1,3]-th_EE[j])^2) #error between demand trajectory and actual trajectory at each time point
  
  for j in 2:n, i in 1:n_mot
      # Angle constraints -> theta_i_dot=d/dt(theta_i)
      JuMP.@NLconstraint(traj, th_dot[i,j] * Δt == th[i,j]-th[i,j-1])
      JuMP.@NLconstraint(traj, th_ddot[i,j] * Δt == th_dot[i,j]-th_dot[i,j-1]) 
  end
  
  #Constraint that creates the optimisation cost function J (aka the variable that we want to minimise)
  JuMP.@NLconstraint(traj, J[1]  == sum(err[j] for j in 1:n))
  
  ## Objective function
  JuMP.@objective(traj, Min, J[1])
  
  ## Solve!
  println("Generating Robot Joint Trajectories...")
  JuMP.optimize!(traj)
  JuMP.solution_summary(traj)
  
  #Create output matrix containing all joint angles for full trajectory
  th_matrix= JuMP.value.(th)[:,:];
  thdot_matrix= JuMP.value.(th_dot)[:,:];
  max_joint_v=maximum(thdot_matrix);
  
  #error vector indicating whether trajectory is possible
  err_vec= JuMP.value.(err)[:];
  y_vec= JuMP.value.(y_EE)[:];
  x_vec= JuMP.value.(x_EE)[:];

  return th_matrix, err_vec, y_vec,x_vec, max_joint_v
end

function calc_princ_evec(th)
  Jacob=ManipJacobian(vec(th))[1:2,:]
  manip_matrix= Jacob*Jacob'
  max_eval_idx=findmax(eigvals(manip_matrix))
  princ_evec=eigvecs(manip_matrix)[:,max_eval_idx[2]]
  return princ_evec, max_eval_idx[1]
end

function Manip_eigen(th)
  Jacob=ManipJacobian(vec(th))[1:2,:]
  manip_matrix= Jacob*Jacob'
  #max_eval_idx=findmax(eigvals(manip_matrix))
  e_vals=eigvals(manip_matrix)
  e_vecs=eigvecs(manip_matrix)
  return e_vecs,e_vals
end

function max_EE_a_x(th)
  #calculates the maximum possible end-effector acceleration in the x-direction, based on given robot joints
  
  J=ManipJacobian(Float64.(th))

  #optimisation problem setup
  prob = JuMP.Model(OSQP.Optimizer)
  JuMP.set_silent(prob)
  JuMP.@variable(prob, -acc_max ≤ thddot[1:n_mot] ≤ acc_max)
  JuMP.@variable(prob, x_acc[1])
  #Objective
  JuMP.@constraint(prob, x_acc[1]==(J*thddot)[1])

  JuMP.@objective(prob, Max, x_acc[1])

  JuMP.optimize!(prob)
  JuMP.solution_summary(prob)

  x_acc_out= JuMP.value.(x_acc);

  return x_acc_out
end

function tip_pos(q::Vector)
  ## Function that calculates the payload tip coordinates, resolved in the moving frame, centred at End-effector
  # Only valid for simple slender beam moving in 2D plane.  
  #q=state (x_EE, y_EE, th_EE, FE angles)

  tip_pos_x=q[1] #End effector tip x-position
  tip_pos_y=q[2] #End effector tip y-position
  for i in 3:ni-1
      tip_pos_x+=0.5*le*(cos(q[i])+cos(q[i+1])) #Estimate of tip x-position
      tip_pos_y+=0.5*le*(sin(q[i])+sin(q[i+1])) #Estimate of tip y-position
  end
  tip_pos_th=q[ni] #Tip angle

  out=[tip_pos_x,tip_pos_y,tip_pos_th]
  return out
end

function tip_vel(x::Vector)
  ## Function that calculates the payload tip coordinates, resolved in the moving frame, centred at End-effector
  # Only valid for simple slender beam moving in 2D plane.  
  #q=state (x_EE, y_EE, th_EE, FE angles)
  q=q_fnc(x)
  qdot=qdot_fnc(x)

  tip_pos_x=qdot[1] #End effector tip x-position
  tip_pos_y=qdot[2] #End effector tip y-position

  for i in 3:ni-1
      tip_pos_x+=0.5*le*(-qdot[i]*sin(q[i])-qdot[i+1]*sin(q[i+1])) #Estimate of tip x-position
      tip_pos_y+=0.5*le*(qdot[i]*cos(q[i])+qdot[i+1]*cos(q[i+1])) #Estimate of tip y-position
  end

  tip_pos_th=qdot[ni] #Tip angle

  out=[tip_pos_x,tip_pos_y,tip_pos_th]
  return out
end


function UR10eFK_dot(x)
  th=q_fnc(x)
  thdot=qdot_fnc(x)

  x_EE_dot = link_l[1]*thdot[1]*cos(th[1])+link_l[2]*(thdot[1]+thdot[2])*cos(th[1]+th[2])+link_l[3]*(thdot[1]+thdot[2]+thdot[3])*cos(th[1]+th[2]+th[3])
  y_EE_dot = -link_l[1]*thdot[1]*sin(th[1])-link_l[2]*(thdot[1]+thdot[2])*sin(th[1]+th[2])-link_l[3]*(thdot[1]+thdot[2]+thdot[3])*sin(th[1]+th[2]+th[3])
  th_EE_dot=thdot[1] + thdot[2] + thdot[3] + thdot[4]

  return [x_EE_dot;y_EE_dot;th_EE_dot]
end