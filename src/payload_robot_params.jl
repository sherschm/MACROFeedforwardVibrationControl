#SYSTEM PARAMETERS. These can be changed.

#using Symbolics
#import LinearAlgebra as LA

payload="Al_APCS_mass"

Ne=2 #number of payload finite elements

mhub=1; #end effector lumped mass

J_ee=0.5*mhub*0.06^2;

robot = "UR10"

## Payload beam properties
Al_desity=2700
width=0.05
depth=0.005
l=1.567

m_beam=1.63 #measured (includes cable mass)
m=3.53 #tip mass - measured
nat_freq_obs=0.64329

E=75*10^9;
EI=E*width*depth^3/12
dens=m_beam/l;
J_tipm=(1/12)*m*(0.06^2+0.042^2); #TO BE CORRECTED!
#I_zz=3.53*1.457^2

no_nodes=Ne+1;
ni=Ne+3 #total number of independant generalised coordinates 

l_vec=Vector{Float64}(undef,Ne)

rho=m_beam/l

g=9.81;

le=l/Ne;
ni=Ne+3
m_dist=zeros(1,2*Ne)
m_dist[1]=mhub;
m_dist[2*Ne]=m;
J_dist=zeros(1,2*Ne)
J_dist[1]=J_ee;
J_dist[2*Ne]=J_tipm;

##Robot Characteristics and limits 
if robot=="UR10"
  nj=4 #number of planar joints
  Fmax = 137.3 #N (14 kg)
  Fxmax = Fmax #N (14 kg)
  Fymax = Fmax #N (14 kg)
  xrange = [-1.5,1.5]
  yrange=[-1,1]
  Tmax= 10 #Nm - should be below 10Nm
  vmax=1.5 #m/s
  acc_max=3
  thmax=6; #radians
  n_mot=4; #number of in-plane actuators
  link_l=[0.612;0.527;0.116]
  max_dist=sum(link_l)
end

robot_lims=(Fmax,Tmax,vmax,thmax,max_dist)


#Generate dynamic model matrices as Julia functions
M, C, K, G=Slender_beam_FEM(l,Ne,m_dist,J_dist,rho,EI,"Func")

