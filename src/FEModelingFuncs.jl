#payload characteristics
using Symbolics
import LinearAlgebra

g=9.81

function Hgen(m1,m2,rho,le)
  # Build H matrix used in "Flexible Robot Dynamics & Control Vol 19, Sect. 3.4"
  H=[rho*le/3+m1 rho*le/6;rho*le/6 rho*le/3+m2];
  return H
end

function Slender_beam_FEM(l,Ne,m_dist,J_dist,rho,EI,out_type)
  ## Creates dynamic model of simple slender payload.  
  ## Derivation adapted from "Flexible Robot Dynamics & Control Vol 19, Sect. 3.4" to include gravity and stiffness
  # Inputs: Ne = No. elements, 
  #         m_dist = vector of masses at each node. 
  #         J_dist = vector of inertia at each node. 
  #         rho    = density of payload PER UNIT LENGTH
  #         EI     = Payload Stiffness parameter (Youngs modulus * 2nd Mom. area)

  le=l/Ne; #element length
  ni=Ne+3 #Number of independant coords (aka FE angles plus (x,y,theta) of the end effector)

  #Step 1
  #Create Symbolic Expressions for Kinematics:

  Symbolics.@variables q[1:(4+4Ne)]
  qdot=Symbolics.scalarize(q[Int(length(q)/2+1):length(q)])
  x1=Symbolics.scalarize(q[1])
  y1=Symbolics.scalarize(q[2])
  theta=Symbolics.scalarize(q[3:(2+2Ne)])
  xdot1=Symbolics.scalarize(q[3+2Ne])
  ydot1=Symbolics.scalarize(q[4+2Ne])
  thetadot=Symbolics.scalarize(q[5+2Ne:(4+4Ne)])

  x_sym=Array{Num}(undef,1,Ne+1) #Symbolic expressions for FE node positions
  y_sym=Array{Num}(undef,1,Ne+1)

  xCOM_sym=Array{Num}(undef,1,Ne) #Symbolic expressions for FE element centre of mass positions
  #yCOM_sym=Array{Num}(undef,1,Ne)

  x_sym[1]=x1;
  y_sym[1]=y1;

  xdot_sym=Array{Num}(undef,1,Ne+1) #Symbolic expressions for FE node velocities
  ydot_sym=Array{Num}(undef,1,Ne+1)
  xdot_sym[1]=xdot1;
  ydot_sym[1]=ydot1;

  U_beam_sym=0;

  for i in 1:Ne
    x_sym[i+1]=x_sym[i]+le*(cos(theta[2i-1])+cos(theta[2i]))/2
    y_sym[i+1]=y_sym[i]+le*(sin(theta[2i-1])+sin(theta[2i]))/2

    xdot_sym[i+1]=xdot_sym[i]-le*(thetadot[2i-1]*sin(theta[2i-1])+thetadot[2i]sin(theta[2i]))/2
    ydot_sym[i+1]=ydot_sym[i]+le*(thetadot[2i-1]*cos(theta[2i-1])+thetadot[2i]cos(theta[2i]))/2

    #Symbolic expression for centre of mass of each fintie element. Used for estimating increase from nominal Grav Potential Energy
    xCOM_sym[i]=x_sym[i]+le*(cos(theta[2i-1])+cos(theta[2i]))/4

    U_beam_sym+=-rho*le*g*xCOM_sym[i]
  end
  #coords of tip mass CoM
  #x_m_COM_sym=x_sym[Ne+1]+ m_height*cos(theta[2*Ne])/2
  #y_m_COM_sym=y_sym[Ne+1]+ m_height*sin(theta[2*Ne])/2
  #xdot_m_COM_sym=xdot_sym[Ne+1]- m_height*thetadot[2*Ne]*sin(theta[2*Ne])/2
  #ydot_m_COM_sym=ydot_sym[Ne+1]+ m_height*thetadot[2*Ne]*cos(theta[2*Ne])/2

  U_sym = U_beam_sym - m_dist[1]*g*x_sym[1] - m_dist[2Ne]*g*x_sym[Ne+1]
  for i in 1:Ne-1
    U_sym+= - m_dist[2*i]*g*x_sym[i+1] - m_dist[2i+1]*g*x_sym[i+1]
  end
  delUdq=Array{Num}(undef,2Ne+2,1) #rows are different x, cols are different thetas

  #Define derivative fucntions (arranged in vectors)

  Dx = Differential(x1)
  Dxdot = Differential(xdot1)
  Dy= Differential(y1)
  Dydot= Differential(ydot1)
  Dth=Vector{Differential}(undef,2Ne)
  Dthdot=Vector{Differential}(undef,2Ne)
  for i in 1:2*Ne
    Dth[i] = Differential(theta[i])
    Dthdot[i] = Differential(thetadot[i])
  end

  delUdq[1] = Dx(U_sym)
  delUdq[2] = Dy(U_sym)
  for i in 3:2Ne+2
    delUdq[i] = Dth[i-2](U_sym)
  end
  
  #Step 2
  delxdq=Array{Num}(undef,Ne+1,2Ne+2) #rows are different x, cols are different thetas
  delydq=Array{Num}(undef,Ne+1,2Ne+2)
  for i in 1:Ne+1
    delxdq[i,1]=Dxdot(xdot_sym[i])
    delydq[i,1]=Dxdot(ydot_sym[i])
    delxdq[i,2]=Dydot(xdot_sym[i])
    delydq[i,2]=Dydot(ydot_sym[i])
    for j in 3:2Ne+2
      #delxdthd[i,j]=expand_derivatives(Dthdot[j](xdot_sym[i]))
      #delydthd[i,j]=expand_derivatives(Dthdot[j](ydot_sym[i]))
      delxdq[i,j]=Dthdot[j-2](xdot_sym[i])
      delydq[i,j]=Dthdot[j-2](ydot_sym[i])
    end
  end

  #Step 3

  H=Array{Float64}(undef,2,2,Ne)
  for i in 1:Ne
    H[:,:,i]=Hgen(m_dist[2*i-1],m_dist[2*i],rho,le)
  end

  #Step 4
  mirs=Array{Num}(undef,Ne,2Ne+2,2Ne+2)
  mirs.=0
  for i in 1:Ne
    for r in 1:2Ne+2
      for s in 1:2Ne+2
        mirs[i,r,s]=([delxdq[i,s] delxdq[i+1,s]]*H[:,:,i]*[delxdq[i,r];delxdq[i+1,r]]+[delydq[i,s] delydq[i+1,s]]*H[:,:,i]*[delydq[i,r];delydq[i+1,r]])[1]
      end
    end
  end

  Jterm=Array{Num}(undef,2Ne+2,2Ne+2)
  Jterm.=0

  for r in 3:2Ne+2
    Jterm[r,r]=J_dist[r-2];
  end

  mrs=Array{Num}(undef,2Ne+2,2Ne+2)
  gvec=Array{Num}(undef,2Ne+2,1)
  mrs.=0
  mrs=dropdims(sum(mirs,dims=1), dims=1)+Jterm

  dMdq=matrix_deriv_and_expand(mrs,q)'
  crs=(kron(qdot',LinearAlgebra.I(2Ne+2))-0.5*kron(LinearAlgebra.I(2Ne+2),qdot'))*dMdq

  for r in 1:size(mrs,1)
    for s in 1:size(mrs,2)
      mrs[r,s]=simplify(expand_derivatives(mrs[r,s]))

      #if r>2&& s>2
       # crs[r,s]=simplify(mrs[r,s]*tan(theta[r-2]-theta[s-2]))
      #end
    end
    gvec[r]=simplify(expand_derivatives(delUdq[r]))
  end

  #ni=Ne+3

  mrs_indep=indep_coord_convert(Ne,mrs,q)
  crs_indep=indep_coord_convert(Ne,crs,q)
  g_indep=indep_coord_convert(Ne,gvec,q)

  #Constrain element angles together using the Null Space Method
  Lam=Array{Float64}(undef,2Ne+2,ni);
  Lam.=0;
  for i in 1:Ne-1
    Lam[2i+2,i+3]=1
    Lam[2i+3,i+3]=1
  end
  Lam[1:3,1:3]=[1 0 0;0 1 0;0 0 1];
  Lam[2Ne+2,ni]=1;

  #Make stiffness matrix:
  #Small angle Approximation used

  Kmat=Array{Float64}(undef,2Ne+2,2Ne+2)
  Kmat.=0
  Kmat[3,3:4]=[1 -1];
  Kmat[2Ne+2,2Ne+1:2Ne+2]=[-1 1];
  for i in 4:2Ne+1
    Kmat[i,i-1]=-1;
    Kmat[i,i]=2; #This is correct, just a weird way of making matrix
    Kmat[i,i+1]=-1;
  end
  Kmat=(EI/(le)).*Kmat;
  K_indep= Lam'*Kmat*Lam; # This is constant, does not need to be a function

  # Create matrix functions 
  #Mmat_f=eval(build_function(Lam'*mrs_indep*Lam,q...)[1])
  #Cmat_f=eval(build_function(Lam'*crs_indep*Lam,q...)[1])
  #G_f=eval(build_function(Lam'*g_indep,q...)[1])
  #=
  writedlm("M_sym.txt", Lam'*mrs_indep*Lam)
  writedlm("C_sym.txt", Lam'*crs_indep*Lam)
  writedlm("K_sym.txt",   K_indep)
  writedlm("G_sym.txt",   Lam'*g_indep)=#

  if out_type=="Sym"
    #return symbolic matrices / vectors
    return Lam'*mrs_indep*Lam, Lam'*crs_indep*Lam, K_indep, Lam'*g_indep
  elseif out_type=="Func"
    #return functions
    Mmat_f=eval(build_function(Lam'*mrs_indep*Lam,q)[1])
    Cmat_f=eval(build_function(Lam'*crs_indep*Lam,q)[1])
    G_f=eval(build_function(Lam'*g_indep,q)[1])
    return Mmat_f, Cmat_f, K_indep, G_f
  end
end

function matrix_deriv_and_expand(Mat,q)
  #produces symbolic matrix of derivatives of input matrix "Mat"

  sz=size(Mat)
  Mat_exp=Array{Num}(undef,sz[1],sz[1]*sz[2])

  for i in 1:sz[1]
    for r in 1:sz[1]
      for s in 1:sz[2]
        Mat_exp[r,sz[2]*(i-1)+s]=simplify(expand_derivatives(Differential(Symbolics.scalarize(q[i]))(Mat[r,s])))
      end
    end
  end

  return Mat_exp
end

function indep_coord_convert(Ne,expr,q)

  expr_indep=expr
  
  for i in 1:Ne-1
    #First set of substitutions
    subs1 = Dict([q[2i+3]=>q[i+3],q[2i+4]=>q[i+4]])#,q[Ne+2i+8]=>q[Ne+i+8],q[Ne+2i+9]=>q[Ne+i+9]])
    expr_indep=substitute(expr_indep,subs1)
  end

  for i in 1:Ne-1
    #same for derivative terms
    subs2 = Dict([q[(2+2Ne)+2i+3]=>q[(2+2Ne)+i+3],q[(2+2Ne)+2i+4]=>q[(2+2Ne)+i+4]])
    expr_indep=substitute(expr_indep,subs2)
  end 

  for i in 1:Ne+3
    #shift all derivative terms
    subs3 = Dict([q[(2+2Ne)+i]=>q[(Ne+3)+i]])
    expr_indep=substitute(expr_indep,subs3)
  end 

  #for i in 1:Ne+3
    #shift down derivative terms
    #subs2 = Dict([q[2Ne+2+i]=>q[Ne+3+i],q[2Ne+2i+4]=>q[i+4],q[Ne+2i+8]=>q[Ne+i+8],q[Ne+2i+9]=>q[Ne+i+9]])
    #expr_indep=substitute(expr_indep,subs2)
  #end 

  return expr_indep
end


function candidate_fn_builder(ni)

  ##assembles candidate functions
  
  Symbolics.@variables q[1:2*ni]

  lin_terms=[]
  for i in 1:2*ni
      append!(lin_terms,q[i])
  end

  M_combos=[]

  #M_combos=kron(sin.(q[3:ni]),cos.(q[3:ni]))

  for i in 3:ni
      append!(M_combos,sin(q[i]))
      append!(M_combos,cos(q[i]))
  end

  for i in 3:ni
      for j in 3:ni
          append!(M_combos,sin(q[i])*sin(q[j]))
          append!(M_combos,sin(q[i])*cos(q[j]))
          append!(M_combos,cos(q[i])*cos(q[j]))
      end
  end


  C_combos=[]
  for i in ni+1:2*ni
      append!(C_combos,q[i]*M_combos)
  end

  candidates=[lin_terms;M_combos;C_combos]

  writedlm("data/candidates.txt", candidates)
end
