##Input Shaping Functions

function RunIS(cmd_traj,cmd_traj_dot,IS_params)

    IS_type = IS_params[1]
    nat_freq=IS_params[2]
    damping=IS_params[3]
    
    cmd_dot_shaped= hcat(input_shaper(2*pi*nat_freq,damping,cmd_traj_dot[:,1],Δt,IS_type),
    input_shaper(2*pi*nat_freq,damping,cmd_traj_dot[:,2],Δt,IS_type),
    input_shaper(2*pi*nat_freq,damping,cmd_traj_dot[:,3],Δt,IS_type))
  
    cmd_shaped=Matrix{Float64}(undef,size(cmd_dot_shaped,1),3)
    cmd_shaped.=0;
    for i in 1:3
        cmd_shaped[:,i]=int_t(cmd_dot_shaped[:,i],Δt).+cmd_traj[1,i]
    end
  
    #convert cmd_traj to cmd_EE (end effector coordinates that would produce the tip motion if payload were rigid)
    cmd_shaped_EE=Array{Float64}(undef,size(cmd_shaped))
    cmd_dot_shaped_EE=Array{Float64}(undef,size(cmd_shaped))
    for i in 1:size(cmd_shaped,1)
        cmd_shaped_EE[i,:]=cmd_shaped[i,:]+[-l*cos(cmd_shaped[i,3]);-l*sin(cmd_shaped[i,3]);0]
        if i>1
        cmd_dot_shaped_EE[i,:]=(cmd_shaped_EE[i,:]-cmd_shaped_EE[i-1,:])./Δt
        end
    end
  
    n_IS=size(cmd_shaped_EE,1)
    tmax_IS=n_IS*Δt
    tvec_IS=0:Δt:(tmax_IS-Δt)
  
    xdes = LinearInterpolation(tvec_IS, cmd_shaped_EE[:,1], extrapolation_bc=Line())
    ydes = LinearInterpolation(tvec_IS, cmd_shaped_EE[:,2],extrapolation_bc=Line())
    thdes = LinearInterpolation(tvec_IS, cmd_shaped_EE[:,3],extrapolation_bc=Line())
  
    xdes_dot = LinearInterpolation(tvec_IS, cmd_dot_shaped_EE[:,1],extrapolation_bc=Line())
    ydes_dot = LinearInterpolation(tvec_IS, cmd_dot_shaped_EE[:,2],extrapolation_bc=Line())
    thdes_dot = LinearInterpolation(tvec_IS, cmd_dot_shaped_EE[:,3],extrapolation_bc=Line())
  
    q_des=(xdes,ydes,thdes)
    q_des_dot=(xdes_dot,ydes_dot,thdes_dot)
  
    pos_matrix, vel_matrix, force_matrix,tvec_control=FEA_PD_SIM(q0,tmax_IS,Δt,M,C,K,G,q_des,q_des_dot)
    tvec=tvec_IS
    tmax=tmax_IS
    cmd_traj=cmd_shaped
    return pos_matrix, vel_matrix, force_matrix,tvec_control, cmd_traj, tvec, tmax
end

function int_t(in,Δt::Float64) 
    #Integrates input wrt to time using Euler method.
    # Used for converting input shaped velocity profile to position command
    out=Vector{Float64}(undef,length(in))
    for j in 1:length(in)
        out[j]=Δt*sum(in[1:j])
    end
 return out
end

function ZV_impulse_fnc(nat_freq::Float64,damping::Float64,Δt::Float64)
    # ZV shaper
    ##INPUTS: Natural frequency and damping ratio. Trajectory position vector. Trajectory time step
   # if active
    K_IS=exp(-damping*pi/sqrt(1-damping^2))
    Δt_IS=pi/(nat_freq*sqrt(1-damping^2))
    n_IS=Int(round(Δt_IS/Δt))
    #Velocity Impulses
    h=zeros(n_IS,1)
    #Impulse 1
    h[1]=1/(1+K_IS)
    #Impulse 2
    h[n_IS]=K_IS*h[1];
    return h
end

function ZVD_impulse_fnc(nat_freq::Float64,damping::Float64,Δt::Float64)
    # ZV shaper
    ##INPUTS: Natural frequency and damping ratio. Trajectory position vector. Trajectory time step
   # if active
   K_IS=exp(-damping*pi/sqrt(1-damping^2))
   Δt_IS=pi/(nat_freq*sqrt(1-damping^2))
   n_IS=Int(round(Δt_IS/Δt))
   #Velocity Impulses
   h=zeros(2*n_IS,1)
   #Impulse 1
   h[1]=1/(1+2*K_IS+K_IS^2);
   #Impulse 2
   h[n_IS]=2*K_IS*h[1];
   #Impulse 3
   h[2*n_IS]=K_IS^2*h[1];
    return h
end

function ZVDD_inpulse_fnc(nat_freq::Float64,damping::Float64,Δt::Float64)
    # ZVDD shaper
    ##INPUTS: Natural frequency and damping ratio. Trajectory position vector. Trajectory time step
   # if active
   K_IS=exp(-damping*pi/sqrt(1-damping^2))
   Δt_IS=pi/(nat_freq*sqrt(1-damping^2))
   n_IS=Int(round(Δt_IS/Δt))
   D_IS = 1+3*K_IS+3*K_IS^2+K_IS^3
   #Velocity Impulses
   h=zeros(3*n_IS,1)
   #Impulse 1
   h[1]=1/D_IS;
   #Impulse 2
   h[n_IS]=3*K_IS*h[1];
   #Impulse 3
   h[2*n_IS]=3K_IS^2*h[1];
   #Impulse 4
   h[3*n_IS]=K_IS^3*h[1];
    return h
end

function input_shaper(nat_freq::Float64,damping::Float64,traj::Vector{Float64},Δt::Float64,IS_type::Int8)
    if IS_type==0
        shaped_vel=traj;
    else #Input Shaping Wanted
        if IS_type==1
            h=ZV_impulse_fnc(nat_freq,damping,Δt)
        elseif IS_type==2
            h=ZVD_impulse_fnc(nat_freq,damping,Δt)
        elseif IS_type==3
            h = ZVDD_inpulse_fnc(nat_freq,damping,Δt)
        end
        #Convolve with traj
        shaped_vel=DSP.conv(traj,h)
    end
    return shaped_vel[1:size(shaped_vel,1),1]
end

function joint_input_shaper(joint_pos_unshpd,joint_vel_unshpd,nat_freq,damping,Δt)
    ##Function that input shapes the optimally generated 

    n_IS=length(input_shaper(2*pi*nat_freq,damping,joint_vel_unshpd[1,:],Δt,Int8(3)))

    shaped_vel=Array{Float64}(undef,n_mot,n_IS)
    shaped_pos=Array{Float64}(undef,n_mot,n_IS)
    for j in 1:n_IS
        for i in nj
            shaped_vel[i,:]=input_shaper(2*pi*nat_freq,damping,joint_vel_unshpd[i,:],Δt,Int8(3))
            shaped_pos[i,:]=int_t(shaped_vel[i,:],Δt).+joint_pos_unshpd[i,1]
        end
    end
    return shaped_pos, shaped_vel
end