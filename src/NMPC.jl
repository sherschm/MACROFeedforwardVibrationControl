#Feedforward Controller

G_reg(x::T...) where {T<:Real} = G(x[2:ni+1])[Int(round(x[1]))]

function dynamics(input...)
    x=collect(input)
    q=x[1:ni]
    qd=x[ni+1:2*ni]
    qdd=x[2*ni+1:3*ni]

    out=M(q)*qdd+C(q,qd)*qd+K*q+G(q)
    return out
end

function MPCoptFEAwithIK(q0,Δt,cmd_traj,n) 
    #This is the Nonlinear Optimisation Solver script, using Julia JuMP library.
    
    #M, C, K, G=dynamics
    #Fmax,Tmax,vmax,thmax,max_dist=robot_lims

    traj = JuMP.Model(Ipopt.Optimizer)
    JuMP.set_silent(traj)
    t_init=time_ns()
    t_end=time_ns()
    t_taken=(t_end-t_init)*10^(-9)

    M0=M(q0)
    C0=C(q0)
    register(traj, :G_reg, ni+1, G_reg, autodiff=true)

    ##Define Optimisation variables
    JuMP.@variables(traj, begin

        # Control variables
        -3 ≤ q[1:ni,1:n] ≤ 3 #generalised coordinates [x;y;th1,th2,th...thN]
        -10 ≤ qdot[1:ni,1:n] ≤ 10 #generalised velocities
        -5 ≤ qddot[1:ni,1:n] ≤ 5 #generalised accelerations
        F_TCP[1:3,1:n] #end-effector forces

        -6 ≤ th[1:nj,1:n] ≤ 6 #joint positions (rad)
        -1.5 ≤ thdot[1:nj,1:n] ≤ 1.5 #joint velocity
        -3 ≤ thddot[1:nj,1:n] ≤ 3 #joint accelerations

        err[1:n] ≥ 0   
        J[1] ≥ 0               #error
    end)
    
    JuMP.@NLexpressions(
        traj,
        begin
            cmdx[j = 1:n],cmd_traj[j,1]
            cmdy[j = 1:n],cmd_traj[j,2]
            cmdth[j = 1:n],cmd_traj[j,3]
        end
    )
    
    ##Initial Condition constraints
    for i in 1:ni
        JuMP.fix(q[i], q0[i]; force = true)
        JuMP.fix(qdot[i], q0[i+ni]; force = true)
    end

    for j in 1:n
        
        ##Dynamic constraints Eq (3a)
        for i in 1:3
            JuMP.@NLconstraint(traj,sum(M0[i,k]*qddot[k,j]+C0[i,k]*qdot[k,j]+K[i,k]*q[k,j] for k in 1:ni)+G_reg(i,q[1:ni,j]...)==F_TCP[i,j])
        end
        for i in 4:ni #Place limits on the bending. required for model validity
            JuMP.@NLconstraint(traj,sum(M0[i,k]*qddot[k,j]+C0[i,k]*qdot[k,j]+K[i,k]*q[k,j] for k in 1:ni)+G_reg(i,q[1:ni,j]...)==0)
        end

        ## Robot kinematic constraints 
        JuMP.@constraint(traj,-0.85*pi<=th[1,j]<=-0.15*pi) #Constrain end effector angle

        #Planar Forward kinematics Eq (3d)
        JuMP.@NLconstraint(traj, q[1,j] == link_l[1]*sin(th[1,j])+link_l[2]*sin(th[1,j]+th[2,j])+link_l[3]*sin(th[1,j]+th[2,j]+th[3,j])) #Equation of end effector position
        JuMP.@NLconstraint(traj, q[2,j] == link_l[1]*cos(th[1,j])+link_l[2]*cos(th[1,j]+th[2,j])+link_l[3]*cos(th[1,j]+th[2,j]+th[3,j])) #Equation of end effector position
        JuMP.@constraint(traj, q[3,j]==th[1,j] + th[2,j] + th[3,j] + th[4,j]) #Constrain end effector angle

        #Force limits
        JuMP.@constraint(traj,-Fmax<=F_TCP[1,j]<=Fmax)
        JuMP.@constraint(traj,-Fmax<=F_TCP[2,j]<=Fmax)
        JuMP.@constraint(traj,-Tmax<=F_TCP[3,j]<=Tmax)

        JuMP.@constraint(traj,-vmax<=qdot[1,j]<=vmax)
        JuMP.@constraint(traj,-vmax<=qdot[2,j]<=vmax)

        #x-y limits of robot
        JuMP.@constraint(traj,-max_dist<=q[1,j]^2+q[2,j]^2<=max_dist)
        for i in 3:ni
            JuMP.@constraint(traj,-thmax<=q[i,j]<=thmax)
        end

        ##Define objective error at every time step
        JuMP.@NLconstraint(traj, err[j] == (cmdy[j]-(q[2,j]+le*(0.5*sin(q[3,j])+sum(sin(q[k,j]) for k in 4:ni-1)+0.5*sin(q[ni,j]))))^2+(cmdx[j]-(q[1,j]+le*(0.5*cos(q[3,j])+sum(cos(q[k,j]) for k in 4:ni-1)+0.5*cos(q[ni,j]))))^2+(cmdth[j]-q[ni,j])^2)
    end

    for j in 2:n
        # Enforce Generalised Coordinate/velocity derivative relations (Forward Euler method)
        
        for i in 1:ni

            ##Forward Euler
            #JuMP.@constraint(traj, qdot[i,j] * Δt == q[i,j]-q[i,j-1])
            #JuMP.@constraint(traj,qddot[i,j] * Δt == qdot[i,j]-qdot[i,j-1])
            
            ##Trapezoidal
            JuMP.@constraint(traj, 0.5*(qdot[i,j]+qdot[i,j-1])*Δt == q[i,j]-q[i,j-1])
            JuMP.@constraint(traj, 0.5*(qddot[i,j]+qddot[i,j-1])*Δt == qdot[i,j]-qdot[i,j-1])
        end
        for i in 1:nj

            ##Forward Euler
            #JuMP.@constraint(traj, thdot[i,j] * Δt == th[i,j] -th[i,j-1] )
            #JuMP.@constraint(traj,thddot[i,j] * Δt == thdot[i,j] -thdot[i,j-1])

            ##Trapezoidal
            JuMP.@NLconstraint(traj, 0.5*(thdot[i,j]+thdot[i,j-1])*Δt == th[i,j]-th[i,j-1])
            JuMP.@NLconstraint(traj, 0.5*(thddot[i,j]+thddot[i,j-1])*Δt == thdot[i,j]-thdot[i,j-1]) 
        end
    end
    
    #Constraint that creates the optimisation cost function J (aka the variable that we want to minimise)
    JuMP.@constraint(traj, J[1]  == sum(err[j] for j in 1:n))

    ## Objective function
    JuMP.@objective(traj, Min, J[1])

    ## Solve!
    println("Generating optimal control inputs")
    t_init=time_ns()

    #JuMP.optimize!(traj)
    JuMP.optimize!(traj)
    t_end=time_ns()
    t_taken=(t_end-t_init)*10^(-9)
    println("Optimisation time (s) = "*string(t_taken))

    JuMP.solution_summary(traj)
    #Create output matrix containing all joint angles for full trajectory
    pos_matrix= JuMP.value.(q)[:,:];
    #Create output matrix containing all joint velocities for full trajectory
    vel_matrix= JuMP.value.(qdot)[:,:];
    force_matrix= JuMP.value.(F_TCP)[:,:];
    err_vec= JuMP.value.(err)[:];    #error vector indicating whether trajectory is possible

    th_matrix=JuMP.value.(th)[:,:];
    thdot_matrix=JuMP.value.(thdot)[:,:];


    return force_matrix, err_vec,pos_matrix,vel_matrix,th_matrix,thdot_matrix
end