## A number of rough scripts used to plot and make animations.
# TO DO:
# - Clean up required

using Plots
using Interpolations

function plot_robot(th,e_val,e_vec,link_l,i)
  #Transpose so that it is n by 4.
  if size(th,2)>4
    th=th'
  end
  gr();
  default(label="");

  #Ideal desired motion
  joint1=[0,0]
  joint2=joint1+link_l[1]*[sin(th[i,1]),cos(th[i,1])]
  joint3=joint2+link_l[2]*[sin(th[i,1]+th[i,2]),cos(th[i,1]+th[i,2])]
  joint4=joint3+link_l[3]*[sin(th[i,1]+th[i,2]+th[i,3]),cos(th[i,1]+th[i,2]+th[i,3])]
  plot(-[joint1[2],joint2[2],joint3[2],joint4[2]],-[joint1[1],joint2[1],joint3[1],joint4[1]],linewidth=10)

  #manipulability principle eigenvec
  plot!(-[joint4[2]-0.4*e_val[i]*e_vec[i,2],joint4[2]+0.4*e_val[i]*e_vec[i,2]],-[joint4[1]-0.4*e_val[i]*e_vec[i,1],joint4[1]+0.4*e_val[i]*e_vec[i,1]],linewidth=1,color = :red,arrow=Plots.Arrow(:open, :both, 2.5, 2.0),label="Manipulability") #Floor
end

function plot_robot(th,link_l,i)
  #Transpose so that it is n by 4.
  if size(th,2)>4
    th=th'
  end
  gr();
  default(label="");

  #Ideal desired motion
  joint1=[0,0]
  joint2=joint1+link_l[1]*[sin(th[i,1]),cos(th[i,1])]
  joint3=joint2+link_l[2]*[sin(th[i,1]+th[i,2]),cos(th[i,1]+th[i,2])]
  joint4=joint3+link_l[3]*[sin(th[i,1]+th[i,2]+th[i,3]),cos(th[i,1]+th[i,2]+th[i,3])]
  plot(-[joint1[2],joint2[2],joint3[2],joint4[2]],-[joint1[1],joint2[1],joint3[1],joint4[1]],linewidth=10)

end

function general_lin_interp(dataset,tvec,tvec_new)
  #data input matrix should be N by X, where N is the time dimension. N can be any size and remains the same.
  interped_data=Array{Float64}(undef,length(tvec_new),size(dataset,2))
  for i in 1:size(dataset,2)
    interp_fn = LinearInterpolation(tvec, dataset[:,i],extrapolation_bc=Line()) 
    interped_data[:,i]=interp_fn(tvec_new)
  end
  return interped_data
end

function FEM_plots(pos_matrix,joint_matrix,force_matrix,cmd_traj,tvec)
  ##Plots/Animations

  tmax=maximum(tvec)
  Δt_anim=0.04  #time step of anmation
  tvec_anim=0:Δt_anim:tmax-Δt_anim;
  fps=Int(round(1/Δt_anim)); #no_frames per sec
  
  #animation axis limits
  y_lim=(-1.2,1.2);
  x_lim=(-1.5,1.7);#Floor constraint

  pos_matrix_anim=general_lin_interp(pos_matrix',tvec,tvec_anim)
  n_anim=size(pos_matrix_anim,1)
  force_matrix_anim=general_lin_interp(force_matrix',tvec,tvec_anim)
  cmd_anim=general_lin_interp(cmd_traj,tvec,tvec_anim)
  
  joint_matrix_anim=general_lin_interp(joint_matrix',tvec,tvec_anim)

  #=e_vec=Array{Float64}(undef,n_anim,2)
  e_val=Vector{Float64}(undef,n_anim)
  for i in 1:n_anim
    e_vec[i,:],e_val[i]=calc_princ_evec(joint_matrix_anim[i,:])
  end=#

  p_Fx=plot(tvec_anim,force_matrix_anim[1:(n_anim),1],linewidth=1.5,xlabel="t (s)",ylabel="Force X (N)", legend=false)
  p_Fy=plot( tvec_anim,-force_matrix_anim[(1:n_anim),2],linewidth=1.5,xlabel="t (s)",ylabel="Force Y  (N)", legend=false)
  p_Torq=plot( tvec_anim,force_matrix_anim[(1:n_anim),3],linewidth=1.5,xlabel="t (s)",ylabel="Torque (Nm)", legend=false)
  plot(p_Fx,p_Fy,p_Torq,layout = (3,1))
  savefig("plots/forces")
  
  p_x=plot( tvec,pos_matrix[1,:],linewidth=1.5,xlabel="t (s)",ylabel="x-position (m)", legend=false)
  p_y=plot( tvec,-pos_matrix[2,:],linewidth=1.5,xlabel="t (s)",ylabel="y-position (m)", legend=false)
  p_th=plot( tvec,pos_matrix[3,:],linewidth=1.5,xlabel="t (s)",ylabel="Angle (rad)", legend=false)
  plot(p_x,p_y,p_th,layout = (3,1))
  savefig("plots/pos_plot")

  plot()
  for i in 1:ni-3
    p_th=plot!( tvec,pos_matrix[3+i,:],linewidth=1.5, xlabel="t (s)",ylabel="Angle (rad)", legend=(i))
    p_th=plot!( tvec,pos_matrix[3+i,:],linewidth=1.5, xlabel="t (s)",ylabel="Angle (rad)", legend=(i))
  end
  savefig("plots/theta_plots")

  plot()
  for i in 1:n_mot
    p_j=plot!( tvec,joint_matrix[i,:],linewidth=1.5, xlabel="t (s)",ylabel="Angle (rad)", legend=(i))
  end
  savefig("plots/joint_angles")

  plot()

  #Create matrix of cartesian coordinates of FEM payload
  nodexpos=Matrix{Float64}(undef,n_anim,no_nodes);
  nodeypos=Array{Float64}(undef,n_anim,no_nodes);
  nodexpos[:,1]=pos_matrix_anim[:,1]
  nodeypos[:,1]=-pos_matrix_anim[:,2]
  for i in 1:(no_nodes-1)
    nodexpos[:,i+1]=nodexpos[:,i].+0.5*le*(cos.(pos_matrix_anim[:,i+2])+cos.(pos_matrix_anim[:,i+3]));
    nodeypos[:,i+1]=(nodeypos[:,i].-0.5*le*(sin.(pos_matrix_anim[:,i+2])+sin.(pos_matrix_anim[:,i+3])));
  end

  p_x_err=plot(tvec_anim,nodexpos[:,no_nodes]-cmd_anim[:,1],xlabel="t (s)",ylabel="end mass position (m)",title="x-Position Error", legend=false)
  p_y_err=plot(tvec_anim,nodeypos[:,no_nodes]+cmd_anim[:,2],xlabel="t (s)",ylabel="end mass position error (m)",title="y-Position Error ", legend=false)
  plot(p_y_err, p_x_err)


  writedlm("data/tstep_study/x_tip_err.csv", [vec(tvec_anim) (nodexpos[:,no_nodes]-cmd_anim[:,1])], ',')
  writedlm("data/tstep_study/y_tip_err.csv", [vec(tvec_anim) (nodeypos[:,no_nodes]+cmd_anim[:,2])], ',')

  
  savefig("plots/error")
    #=
  if ctrl_law=="PD" || ctrl_law=="InputShaping"
    xerr=nodexpos[:,no_nodes]-cmd_anim[:,1]
    yerr=nodeypos[:,no_nodes]+cmd_anim[:,2]
    
    if traj_mode=="ramp"
      err=sqrt.(xerr.^2+yerr.^2) #xy position error
      ErrTraj=DataFrame([tvec_anim err], :auto)
      CSV.write("data/TipError.csv", ErrTraj,header=false)
    elseif traj_mode=="Comp_ramp"
      err=sqrt.(xerr.^2+yerr.^2) #xy position error
      therr=pos_matrix_anim[:,ni]+cmd_anim[:,3]
      ErrTraj=DataFrame([tvec_anim err therr], :auto)
      CSV.write("data/TipError.csv", ErrTraj,header=false)
    end
  end
    =#
  ## Create animation
  println("Generating animation...")

  rectangle(w, h, x, y) = Shape(x.+ [0,w,w,0], y.+ [0,0,h,h]) #AIM-TU table

  anim = @animate for i in 1:n_anim
   
    #plot_robot(joint_matrix_anim,e_val,e_vec,link_l,i)
    plot_robot(joint_matrix_anim,link_l,i)
    #Plot Modelled payload motion
    scatter!([nodeypos[i,1]],[-nodexpos[i,1]],markersize = 10, aspect_ratio=:equal, label="End-effector")
    scatter!([nodeypos[i,no_nodes]],[-nodexpos[i,no_nodes]],markersize = 7, aspect_ratio=:equal, label="Tip mass")
    plot!([nodeypos[i,1],nodeypos[i,2]],[-nodexpos[i,1],-nodexpos[i,2]],linewidth=3,label=false)
    plot!([-1.2,1.2],[-1,-1],linewidth=1,color = :black,label=false) #Floor
    #plot!([0.3,0.3],[-1,1.5],linewidth=1,color = :blue,label="Tip Barrier") #Wall
    plot!(rectangle(1.6,1,-0.8,-1), color = :gray, opacity=.1)
    plot!([nodeypos[i,1]-0.1*cos(pos_matrix_anim[i,3]),nodeypos[i,1]+0.1*cos(pos_matrix_anim[i,3])],[-nodexpos[i,1]+0.1*sin(pos_matrix_anim[i,3]),-nodexpos[i,1]-0.1*sin(pos_matrix_anim[i,3])],linewidth=1,color = :black,label=false)
    plot!([nodeypos[i,1]+0.15*sin(pos_matrix_anim[i,3]),nodeypos[i,1]-0.1*sin(pos_matrix_anim[i,3])],[-nodexpos[i,1]+0.15*cos(pos_matrix_anim[i,3]),-nodexpos[i,1]-0.1*cos(pos_matrix_anim[i,3])],linewidth=1,color = :black,label=false)
    for j in 2:no_nodes-1
      plot!([nodeypos[i,j],nodeypos[i,j+1]],[-nodexpos[i,j],-nodexpos[i,j+1]],linewidth=3,label=false)
      scatter!([nodeypos[i,j]],[-nodexpos[i,j]],markersize = 3, aspect_ratio=:equal,xlim=y_lim, ylim=x_lim)
    end
    scatter!([-cmd_anim[i,2]],[-cmd_anim[i,1]],markersize = 10, aspect_ratio=:equal, label="Desired end point",shape = :+,color = :black,legend=:topleft)

  end
  gif(anim,"plots/FEMpayload.gif",fps=fps);

  ## Ramp Trajectory Plot
  plot(tvec_anim,nodeypos[:,size(nodeypos,2)], linewidth=2,label="Simulation")
  plot!(tvec,-cmd_traj[:,2],xlabel="t (s)", linewidth=2,linestyle=:dash,ylabel="Tip Y - position (m)", label="Desired",legend=:bottomright)
  savefig("plots/ytraj")

  plot(tvec_anim,nodexpos[:,size(nodeypos,2)], linewidth=2,label="Simulation")
  plot!(tvec,cmd_traj[:,1],xlabel="t (s)", linewidth=2,linestyle=:dash,ylabel="Tip X - position (m)", label="Desired",legend=:bottomright)
  savefig("plots/xtraj")
  plot(tvec_anim,pos_matrix_anim[:,ni], linewidth=2,label="Simulation")
  plot!(tvec,cmd_traj[:,3],xlabel="t (s)", linewidth=2,linestyle=:dash,ylabel="Tip Angle (rad)", label="Desired",legend=:bottomright)
  savefig("plots/thtraj")

  ## Plot deflection
  plot(tvec_anim,nodeypos[:,size(nodeypos,2)]+cmd_anim[:,2], linewidth=2,ylabel="Deflection (m)",xlabel="Time (s)")
  savefig("plots/deflection")
end

##Error plotter!
function Error_comparison()

  Err_opt_d = DataFrame(CSV.File("data/2023_report_plots/Compound/TipError_opt.csv"))
  Err_PD_d = DataFrame(CSV.File("data/2023_report_plots/Compound/TipError_PD.csv"))
  Err_IS_d = DataFrame(CSV.File("data/2023_report_plots/Compound/TipError_IS.csv"))

  Err_opt= Matrix(Err_opt_d)
  Err_PD= Matrix(Err_PD_d)
  Err_IS= Matrix(Err_IS_d)
  plot()
  plot!(Err_opt[:,1],Err_opt[:,2],linewidth=1.5, label="Optimal",xlabel="t (s)",ylabel="Tip Position Error (m)",)
  plot!(Err_PD[:,1],Err_PD[:,2],linewidth=1.5,linestyle=:dot,label="No Shaping" )
  plot!(Err_IS[:,1],Err_IS[:,2],linewidth=1.5,linestyle=:dash,label="ZVD input shaping" )
  savefig("plots/xy_err_comparison")

  plot()
  plot!(Err_opt[:,1],Err_opt[:,3],linewidth=1.5, label="Optimal",xlabel="t (s)",ylabel="Tip Angle Error (rad)",)
  plot!(Err_PD[:,1],Err_PD[:,3],linewidth=1.5,linestyle=:dot,label="No Shaping" )
  plot!(Err_IS[:,1],Err_IS[:,3],linewidth=1.5,linestyle=:dash,label="ZVD input shaping" )
  savefig("plots/th_err_comparison")
end


function robustness_plots()

  Err_perf_d = DataFrame(CSV.File("data/2023_report_plots/Horizontal/Robustness/0_9xDensity/errMPC2.csv"))
  y_perf_d = DataFrame(CSV.File("data/2023_report_plots/Horizontal/Robustness/0_9xDensity/y_vecMPC2.csv"))
  Err_1_1xdensity_d = DataFrame(CSV.File("data/2023_report_plots/Horizontal/Robustness/0_9xDensity/err.csv"))
  y_1_1xdensity_d = DataFrame(CSV.File("data/2023_report_plots/Horizontal/Robustness/0_9xDensity/y_vec.csv"))
  Err_0_9xdensity_d = DataFrame(CSV.File("data/2023_report_plots/Horizontal/Robustness/0_9xDensity/err.csv"))
 # y_0_9xdensity_d = DataFrame(CSV.File("data/2023_report_plots/Horizontal/Robustness/0_9xDensity/y_vec.csv"))


  Err_perf= Matrix(Err_perf_d)
  y_perf= Matrix(y_perf_d)
  Err_1_1xdensity= Matrix(Err_1_1xdensity_d)
  y_1_1xdensity= Matrix(y_1_1xdensity_d)
 Err_0_9xdensity= Matrix(Err_0_9xdensity_d)
  y_0_9xdensity= Matrix(y_0_9xdensity_d)

  plot()
  plot!(Err_perf[:,1],Err_perf[:,2],linewidth=1.5, label="MPC",xlabel="t (s)",ylabel="Tip Position Absolute Error (m)",)
  plot!(Err_1_1xdensity[:,1],Err_1_1xdensity[:,2],linewidth=1.5,linestyle=:dot,label="Offline" )
  #plot!(Err_0_9xdensity[:,1],Err_0_9xdensity[:,2],linewidth=1.5,linestyle=:dash,label="-10% Density Uncertainty" )
  savefig("plots/err_robustness3")

  #=
  ydes = LinearInterpolation(tvec, cmd_traj[:,2],extrapolation_bc=Line())
  plot()
  plot!(tvec_anim,-ydes(tvec_anim),linewidth=1.5,linestyle=:dashdot,label="Command trajectory")
  plot!(y_perf[:,1],y_perf[:,2],linewidth=1.5, label="No Uncertainty",xlabel="t (s)",ylabel="Tip y-position (m)")
  plot!(y_1_1xdensity[:,1],y_1_1xdensity[:,2],linewidth=1.5,linestyle=:dot,label="+10% Density Uncertainty" )
  plot!(y_0_9xdensity[:,1],y_0_9xdensity[:,2],linewidth=1.5,linestyle=:dash,label="-10% Density Uncertainty" ,legend=:bottomright)
  savefig("plots/y_robustness")=#
end

function plot_y_commands()
  #plot(tvec,-cmd_traj[:,2],linewidth=1.5, xlabel="t (s)", ylims=(-0.1,0.9),ylabel="Tip y-position (m)",label=false,aspect_ratio=2.0)
  #savefig("plots/y_traj")
  plot(tvec,-cmd_traj_dot[:,2],linewidth=1.5, xlabel="t (s)", ylims=(-0.1,0.6),ylabel="Tip y-velocity (m/s)",label=false,aspect_ratio=2.0)
  savefig("plots/y_dot_traj")
end

##Export y traj
function Export_robustness_data()
  #Used for the end of 2022 APCS report... Keep for now.
  ydes = LinearInterpolation(tvec, cmd_traj[:,2],extrapolation_bc=Line())
  
  error=ydes(tvec_robot)-y_vec
  err_norm=Vector{Float64}(undef,length(error))

  for j in 1:length(error)
    err_norm[j]=error[j]
    if err_norm[j]<0
      err_norm[j]=-error[j]
    end
  end
  dummy=DataFrame([tvec_robot err_norm], :auto)
  CSV.write("data/2023_report_plots/Horizontal/err.csv", dummy,header=false)

  dummy=DataFrame([tvec_robot -y_vec], :auto)
  CSV.write("data/2023_report_plots/Horizontal/y_vec.csv", dummy,header=false)
end

##Export time series data into CSV (for report plotting)
function Export_data()
  if ctrl_law == "Optimal_Ffwd"
    for i in 1:n
      if err[i]<0
        err[i]=0
      else
        err[i]=sqrt(err[i])
      end
    end
    if traj_mode =="ramp"
      ErrTraj=DataFrame([tvec err ], :auto)
    elseif traj_mode =="Comp_ramp"
      therr=pos_matrix[ni,:]-cmd_traj[:,3]
      ErrTraj=DataFrame([tvec err therr], :auto)
    end
    CSV.write("data/TipError.csv", ErrTraj,header=false)

    #End effector forces
    ForceTraj=DataFrame([tvec force_matrix[1,:] -force_matrix[2,:] force_matrix[3,:]], :auto)
    CSV.write("data/Forces.csv", ForceTraj,header=false)
  end
end

#=
#write cartesian command array for Lucas

velocity_array=DataFrame([tvec cmd_traj_dot], :auto)
CSV.write("data/velocity_array.csv", velocity_array,header=false)

pos_array=DataFrame([tvec cmd_traj], :auto)
CSV.write("data/position_array.csv", pos_array,header=false)
=#

#FFT stuff...

#=
sig=vec(pos_matrix_anim[:,ni])

# Fourier Transform of it 
F = fft(sig) |> fftshift
freqs = fftfreq(length(tvec_anim), fps) |> fftshift

# plots 
freq_domain = plot(freqs, abs.(F), title = "Spectrum", ylim=(0, 200),xlim=(0, maximum(fn_analytical))) 
#plot(time_domain, freq_domain, layout = 2)
#plot(freq_domain)
for i in 1:3
  vline!([fn_analytical[i]], label= "Analytical",line=(:dot, 2))
end
display(freq_domain)
savefig("plots/FFT.png")
=#


function plot_changing_timestep()
  #using DelimitedFiles
  #using LaTeXStrings
  data0_1s = readdlm("data/tstep_study/0_1s/y_tip_err.csv", ',', Float64)
  data0_2s = readdlm("data/tstep_study/0_2s/y_tip_err.csv", ',', Float64)
  data0_05s = readdlm("data/tstep_study/0_05s/y_tip_err.csv", ',', Float64)
  data0_025s = readdlm("data/tstep_study/0_025s/y_tip_err.csv", ',', Float64)

  plot(data0_025s[:,1],data0_025s[:,2],linewidth=1.5,label=L"\Delta t"*"=0.025s",size=(500,220), dpi=300,ylims=(-0.09,0.05),xtickfont=font(8), ytickfont=font(8))
  plot!(data0_05s[:,1],data0_05s[:,2],linestyle=:dashdot,linewidth=1.5,label=L"\Delta t"*"=0.05s",xguidefont=font(10),yguidefont=font(11))
  plot!(data0_1s[:,1],data0_1s[:,2],linestyle=:dash,linewidth=1.5,label=L"\Delta t"*"=0.1s")
  plot!(data0_2s[:,1],data0_2s[:,2],linestyle=:dot,linewidth=1.5,label=L"\Delta t"*"=0.2s",ylabel=L"y_{tip,d}-y_{tip} (m)",xlabel="Time (s)", legendfontsize=7)
  savefig("data/tstep_study/tstep_plot")

end